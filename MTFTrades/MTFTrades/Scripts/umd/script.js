(function($) { 

	"use strict";
	
	/* ================ Revolution Slider. ================ */
	if($('.tp-banner').length > 0){
		$('.tp-banner').show().revolution({
			delay:6000,
	        startheight:550,
	        startwidth: 1170,
	        hideThumbs: 1000,
	        navigationType: 'none',
	        touchenabled: 'on',
	        onHoverStop: 'on',
	        navOffsetHorizontal: 0,
	        navOffsetVertical: 0,
	        dottedOverlay: 'none',
	        fullWidth: 'on'
		});
	}
	if($('.tp-banner-full').length > 0){
		$('.tp-banner-full').show().revolution({
			delay:6000,
	        hideThumbs: 1000,
	        navigationType: 'none',
	        touchenabled: 'on',
	        onHoverStop: 'on',
	        navOffsetHorizontal: 0,
	        navOffsetVertical: 0,
	        dottedOverlay: 'none',
	        fullScreen: 'on'
		});
	}
	

	if ($('.partner-logo-slide.owl-carousel').length > 0) {
		$('.partner-logo-slide.owl-carousel').owlCarousel({
			loop: true,
			items: 6,
			autoplay: true,
			nav: true,
			dots: false,
			navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			responsive: {
				0: {
					items: 3
				},
				768: {
					items: 4
				},
				1024: {
					items: 6
				}
			}
		});
	}
	if ($('.image-slider.owl-carousel').length > 0) {
		$('.image-slider.owl-carousel').owlCarousel({
			loop: true,
			items: 1,
			autoplay: true,
			nav: true,
			dots: false,
			navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			responsive: {
				0: {
					items: 1
				},
				768: {
					items: 1
				},
				1024: {
					items: 1
				}
			}
		});
	}

	if ($('.popup-link').length > 0) {
		$('.popup-link').magnificPopup({
			type: 'image',
			gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1]
					},	
			});
	}



/* ================ testimonials ================ */
$(document).ready(function() { 
  	$(".owl-carousel").owlCarousel({       
	   loop:true,
		margin:10,
		nav:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			700:{
				items:2,
				nav:false
			},
			1170:{
				items:2,
				nav:true,
				loop:false
			}
		}
	  
	  
  	}); 

});

})(jQuery);