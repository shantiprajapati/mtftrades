<%
    response.write(Server.MapPath("\") & "<br>")
 %>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow">
	<title>File Manager Integration<script src="admin/ckeditor/ckeditor.js"></script></title>
     
	<script src="https://cdn.ckeditor.com/4.10.1/full/ckeditor.js"></script>
	
	 
</head>

<body>

	<textarea cols="10" id="editor1" name="editor1" rows="10" > 
	</textarea>

	<script>
		CKEDITOR.replace( "editor1", {
			height: 300,

			// Configure your file manager integration. This example uses CKFinder 3 for PHP.
			filebrowserBrowseUrl: "admin/ckeditor/ckfinder/ckfinder.html",
			filebrowserImageBrowseUrl: "admin/ckeditor/ckfinder/ckfinder.html?type=Images",
			filebrowserUploadUrl: "admin/ckeditor/ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Files",
			filebrowserImageUploadUrl: "admin/ckeditor/ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Images"
		} );
	</script>
</body>

</html>