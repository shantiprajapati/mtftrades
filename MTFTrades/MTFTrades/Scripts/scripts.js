var MORE = "... More...",
    LESS = " Less...";

$(function(){
    $("article").each(function(){
        var $ths = $(this),
            txt = $ths.text();

        //Clear the text
        $ths.text("");

        //First 100 chars
        $ths.append($("<article>").text(txt.substr(0,500)));

        //The rest
        $ths.append($("<article>").text(txt.substr(500, txt.length)).hide());

        //More link
        $ths.append(
            $("<a>").text(MORE).click(function(){
                var $ths = $(this);

                if($ths.text() == MORE){
                    $ths.prev().show();
                    $ths.text(LESS);
                }
                else{
                    $ths.prev().hide();
                    $ths.text(MORE);
                }
            })
        );
    });
});
$(document).ready(function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#RequestQuote').offset().top - 100
    }, 'slow');
});

