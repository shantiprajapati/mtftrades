﻿using MTFTrades.Models;
using MTFTrades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MTFTrades.Controllers
{
    [RoutePrefix("api/MTFTAPI")]
    public class MTFTAPIController : ApiController
    {
        MHTEntities _MHTDatabase = new MHTEntities();

        #region Sign Up Distributer
        [HttpPost]
        [ActionName("SignUp_Distributer")]
        public List<VM_SignUp> SignUp_Distributer(VM_SignUp model)
        {
            List<VM_SignUp> DistributerInfo = new List<VM_SignUp>();
            try
            {
                bool sponcerStatus = CheckSponcerID(model.SponcerID);
                if(sponcerStatus)
                {
                    int DistributerCount = CountDistributer("Distributer");
                    MHT_Distributer distributer = new MHT_Distributer()
                    {
                        DistributerID=DistributerCount,
                        FirstName=model.FirstName,
                        LastName=model.LastName,
                        Mobile=model.MobileNumber,
                        ParentID=Convert.ToInt32(model.SponcerID),
                        Password=model.Password
                    };
                    _MHTDatabase.MHT_Distributer.Add(distributer);
                    _MHTDatabase.SaveChanges();
                    var paramupdate = _MHTDatabase.MHT_Parameter.Where(x => x.Parameter == "Distributer").FirstOrDefault();
                    if(paramupdate!=null)
                    {
                        paramupdate.Value = DistributerCount;
                        _MHTDatabase.SaveChanges();
                    }
                    var info = _MHTDatabase.MHT_Distributer.Where(x => x.DistributerID == DistributerCount).FirstOrDefault();
                    if(info!=null)
                    {
                        VM_SignUp tm = new VM_SignUp()
                        {
                            SponcerID=info.DistributerID.ToString(),
                            Password=info.Password
                        };
                        DistributerInfo.Add(tm);
                    }
                }
                  
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }

        private int CountDistributer(string v)
        {
            int distributer = 0;
            try
            {
                var query = _MHTDatabase.MHT_Parameter.Where(x => x.Parameter == v).FirstOrDefault();
                if(query!=null)
                {
                    distributer = Convert.ToInt32(query.Value) + 1;
                }
            }
            catch(Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return distributer;
        }

        private bool CheckSponcerID(string sponcerID)
        {
            bool status = false;
            try
            {
                int distID = Convert.ToInt32(sponcerID);
                var query = _MHTDatabase.MHT_Distributer.Where(x => x.DistributerID == distID).FirstOrDefault();
                if(query!=null)
                {
                    status = true;
                }
            }
            catch(Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return status;
        }
        #endregion

        #region Login Distributer Return Only Name
        [HttpPost]
        [ActionName("Login_Distributer")]
        public List<MHT_Distributer> Login_Distributer(VM_Login model)
        {
            List<MHT_Distributer> DistributerInfo = new List<MHT_Distributer>();
            try
            {
                int distID = Convert.ToInt32(model.SponcerID);
                var dislist = _MHTDatabase.MHT_Distributer.Where(x => x.DistributerID == distID && x.Password == model.Password).FirstOrDefault();
                if(dislist!=null)
                {
                    MHT_Distributer dtinfo = new MHT_Distributer()
                    {
                       FirstName=dislist.FirstName,
                       LastName=dislist.LastName,
                       DistributerID=dislist.DistributerID
                    };
                    DistributerInfo.Add(dtinfo);
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
        #endregion

        #region Login Admin User
        [HttpPost]
        [ActionName("Login_Admin")]
        public List<MHT_Admin> Login_Admin(VM_AdminLogin model)
        {
            List<MHT_Admin> DistributerInfo = new List<MHT_Admin>();
            try
            {
               
                var dislist = _MHTDatabase.MHT_Admin.Where(x => x.UserName == model.UserName && x.Password == model.Password).FirstOrDefault();
                if (dislist != null)
                {
                    MHT_Admin dtinfo = new MHT_Admin()
                    {
                        UserName = dislist.UserName,
                        Password = dislist.Password,
                        AdminID = dislist.AdminID
                    };
                    DistributerInfo.Add(dtinfo);
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
        #endregion

        #region Add/Edit Category
        [HttpPost]
        [ActionName("Add_Edit_Category")]
        public List<MHT_Category> Add_Edit_Category(VM_Category model)
        {
            List<MHT_Category> DistributerInfo = new List<MHT_Category>();
            try
            {
                if(model.CategoryID==0)
                {
                    int categoryCount = CountDistributer("Category");
                    MHT_Category stm = new MHT_Category()
                    {
                        CategoryID=categoryCount,
                        Name=model.CategoryName,
                        IsActive=model.IsActive
                    };
                    _MHTDatabase.MHT_Category.Add(stm);
                    _MHTDatabase.SaveChanges();
                    var paramupdate = _MHTDatabase.MHT_Parameter.Where(x => x.Parameter == "Category").FirstOrDefault();
                    if (paramupdate != null)
                    {
                        paramupdate.Value = categoryCount;
                        _MHTDatabase.SaveChanges();
                    }
                }
                else
                {
                    var query = _MHTDatabase.MHT_Category.Where(x => x.CategoryID == model.CategoryID).FirstOrDefault();
                    if(query!=null)
                    {
                        query.Name = model.CategoryName;
                        query.IsActive = model.IsActive;
                        _MHTDatabase.SaveChanges();
                    }
                }
              
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
        #endregion

        #region Listing Category
        [HttpPost]
        [ActionName("List_Category")]
        public List<MHT_Category> List_Category(VM_Category model)
        {
            List<MHT_Category> DistributerInfo = new List<MHT_Category>();
            try
            {
                var catlist = _MHTDatabase.MHT_Category.OrderBy(x => x.Name).ToList();
                for(int i=0;i<catlist.Count;i++)
                {
                    MHT_Category itm = new MHT_Category()
                    {
                        CategoryID=catlist[i].CategoryID,
                        Name=catlist[i].Name,
                        IsActive=catlist[i].IsActive
                    };
                    DistributerInfo.Add(itm);
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
        #endregion

        #region Add/Edit Category
        [HttpPost]
        [ActionName("Add_Edit_SubCategory")]
        public List<MHT_SubCategory> Add_Edit_SubCategory(VM_SubCategory model)
        {
            List<MHT_SubCategory> DistributerInfo = new List<MHT_SubCategory>();
            try
            {
                if (model.SubCategoryID == 0)
                {
                    int categoryCount = CountDistributer("SubCategory");
                    MHT_SubCategory stm = new MHT_SubCategory()
                    {
                        SubCategoryID = categoryCount,
                        Name = model.SubCategoryName,
                        IsActive =Convert.ToBoolean( model.IsActive),
                        CategoryID=Convert.ToInt32( model.CategoryID)
                    };
                    _MHTDatabase.MHT_SubCategory.Add(stm);
                    _MHTDatabase.SaveChanges();
                    var paramupdate = _MHTDatabase.MHT_Parameter.Where(x => x.Parameter == "SubCategory").FirstOrDefault();
                    if (paramupdate != null)
                    {
                        paramupdate.Value = categoryCount;
                        _MHTDatabase.SaveChanges();
                    }
                }
                else
                {
                    var query = _MHTDatabase.MHT_SubCategory.Where(x => x.SubCategoryID == model.SubCategoryID).FirstOrDefault();
                    if (query != null)
                    {
                        query.Name = model.SubCategoryName;
                        query.IsActive = Convert.ToBoolean(model.IsActive);
                        query.CategoryID = Convert.ToInt32(model.CategoryID);
                        _MHTDatabase.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
        #endregion

        #region Listing Category
        [HttpPost]
        [ActionName("List_SubCategory")]
        public List<MHT_SubCategory> List_SubCategory(VM_SubCategory model)
        {
            List<MHT_SubCategory> DistributerInfo = new List<MHT_SubCategory>();
            try
            {
                var catlist = _MHTDatabase.MHT_SubCategory.OrderBy(x => x.Name).ToList();
                for (int i = 0; i < catlist.Count; i++)
                {
                    MHT_SubCategory itm = new MHT_SubCategory()
                    {
                        CategoryID = catlist[i].CategoryID,
                        Name = catlist[i].Name,
                        IsActive = catlist[i].IsActive,
                        SubCategoryID=catlist[i].SubCategoryID
                    };
                    DistributerInfo.Add(itm);
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
        #endregion
    }
}
