﻿using MTFTrades.CommonClasses;
using MTFTrades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MTFTrades.Controllers
{
    public class HomeController : Controller
    {
        CallAPI _CallAPI = new CallAPI();
       
        public ActionResult Index()
        {
            return View();
        }

        #region Distributer Sign Up
        public ActionResult SignUp(int? id)
        {
            if(id!=null)
            {
                int st = Convert.ToInt32(id);
                if(st==1)
                {
                    ViewBag.DistributerID = "Your ID :" + Session["DistributerID"];
                    ViewBag.Password = "And Password :" + Session["Password"];
                }
                else
                {
                    ViewBag.DistributerID = null;
                    ViewBag.Errors = "Invalid Sponcer ID, Try Again..";
                    ViewBag.Password =null;
                }
               
            }
            else
            {
                Session["DistributerID"] = null;
                Session["Password"] = null;
                ViewBag.DistributerID = "";
                ViewBag.Password ="";
            }
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(VM_SignUp model)
        {
            if(ModelState.IsValid)
            {
                var distributerinfo = _CallAPI.SignUpNewDistributer(model);
                if(distributerinfo.Count!=0)
                {
                    Session["DistributerID"] = distributerinfo[0].SponcerID;
                    Session["Password"] = distributerinfo[0].Password;
                    return RedirectToAction("SignUp", new { id = 1 });
                }
                else
                {
                    return RedirectToAction("SignUp", new { id =0 });
                }
               
            }
            else
            {
                return PartialView("SignUp");
            }
        }
        #endregion

        #region Distributer Login
        public ActionResult Login(int? id)
        {
            if(id!=null)
            {
                int stt = Convert.ToInt32(id);
                if(id==1)
                {
                    return RedirectToAction("DashBoard");
                }
                else
                {
                    ViewBag.LoginError ="Sponcer ID or Password Invalid. Try Again...";
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(VM_Login model)
        {
            if(ModelState.IsValid)
            {
                var checkData = _CallAPI.LoginDistributer(model);
                if(checkData.Count!=0)
                {
                    Session["DistributerName"] =checkData[0].FirstName + " " + checkData[0].LastName;
                    return RedirectToAction("Login", new { id = 1 });
                }
                else
                {
                    return RedirectToAction("Login", new { id = 0 });
                }
               
            }
            else
            {
                return PartialView("Login");
            }
        }
        #endregion

        #region Distributer Dash-Board
        public ActionResult DashBoard()
        {
            ViewBag.DistributerName ="Hello,"+ Session["DistributerName"];
            return View();
        }
        #endregion
    }
}