﻿using MTFTrades.CommonClasses;
using MTFTrades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MTFTrades.Controllers
{
    public class HomeLayoutController : Controller
    {
        CallAPI _CallAPI = new CallAPI();
        public ActionResult PartialPageFooter()
       {
            return PartialView("PartialPageFooter");
       }

        public ActionResult PartialPageHeader()
        {
            return PartialView("PartialPageHeader");
        }

        public ActionResult PartialPageSliders()
        {
            return PartialView("PartialPageSliders");
        }

        public ActionResult PartialCategoryList()
        {
            VM_Category model = new VM_Category()
            {
                CategoryID=0
            };
            var catlist = _CallAPI.ListCategory(model);
            ViewBag.CategoryList = catlist;
            return PartialView("PartialCategoryList");
        }

        public ActionResult EditCategory(int id)
        {
            return RedirectToAction("Category","Admin", new { id = id });
        }

        public ActionResult DeleteCategory(int id)
        {
            return RedirectToAction("Category","Admin", new { id = id });
        }

        public ActionResult PartialSubCategoryList()
        {
            VM_SubCategory model = new VM_SubCategory()
            {
                SubCategoryID = 0
            };
            var catlist = _CallAPI.ListSubCategory(model);
            ViewBag.SubCategoryList = catlist;
            return PartialView("PartialSubCategoryList");
        }
    }
}