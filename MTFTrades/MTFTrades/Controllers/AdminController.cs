﻿using MTFTrades.CommonClasses;
using MTFTrades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MTFTrades.Controllers
{
    public class AdminController : Controller
    {
        CallAPI _CallAPI = new CallAPI();
        #region Admin Login

        public ActionResult Admin(int? id)
        {
            if(id!=null)
            {
                ViewBag.LoginError = "Your UserName or Password Invalid.";

            }
            else
            {
                ViewBag.LoginError = "";
            }
            return View();
        }

        [ActionName("2018-2099")]
        public ActionResult _2018_2099()
        {
            return View("Admin");
        }

        [HttpPost]
        public ActionResult Admin(VM_AdminLogin model)
        {
            if(ModelState.IsValid)
            {
                var admninfo = _CallAPI.LoginAdmin(model);
                if(admninfo.Count!=0)
                {
                    Session["AdminID"] = admninfo[0].AdminID;
                    return RedirectToAction("DashBoard");
                }
                else
                {
                    Session["AdminID"] = null;
                    return RedirectToAction("Admin", new { id = 0 });
                }
               
            }
            else
            {
                return PartialView("Admin");
            }
        }
        #endregion

        public ActionResult DashBoard()
        {
            if (Session["AdminID"] == null)
                return View("Admin");
            else
                return View();
        }

        #region Manage Category

        public ActionResult Category(int? id)
        {
            VM_Category catinfo = new VM_Category();
            if (Session["AdminID"] == null)
                return View("Admin");
            else
            {
                if(id!=null)
                {
                    if(Convert.ToInt32(id)==0)
                    {
                        catinfo.IsActive = false;
                        catinfo.CategoryName = "";
                        catinfo.CategoryID = 0;
                        return View(catinfo);
                    }
                    else
                    {
                        VM_Category itt = new VM_Category()
                        {
                            CategoryID = Convert.ToInt32(id)
                        };
                        var catdata = _CallAPI.ListCategory(itt);
                        for (int i = 0; i < catdata.Count; i++)
                        {
                            if (catdata[i].CategoryID == Convert.ToInt32(id))
                            {
                                catinfo.CategoryID = Convert.ToInt32(catdata[i].CategoryID);
                                catinfo.CategoryName = catdata[i].Name;
                                catinfo.IsActive = Convert.ToBoolean(catdata[i].IsActive);
                            }
                        }
                        return View(catinfo);
                    }
                  
                }
                
            }
            catinfo.IsActive = false;
            catinfo.CategoryName = "";
            catinfo.CategoryID = 0;
            return View(catinfo);
        }
        [HttpPost]
        public ActionResult Category(VM_Category model)
        {
            if(ModelState.IsValid)
            {
                var mngCategory = _CallAPI.ManageCategory(model);
                return RedirectToAction("Category",new { id=0});
            }
            else
            {
                return PartialView("Category");
            }
        }

        #endregion

        #region Manage Sub Category
        public ActionResult SubCategory(int? id)
        {
            VM_SubCategory catinfo = new VM_SubCategory();
            if (Session["AdminID"] == null)
                return View("Admin");
            else
            {
                if (id != null)
                {
                    if (Convert.ToInt32(id) == 0)
                    {
                       
                        catinfo.SubCategoryName = "";
                        catinfo.CategoryID = "0";
                        catinfo.SubCategoryID = 0;
                        return View(catinfo);
                    }
                    else
                    {
                        VM_SubCategory itt = new VM_SubCategory()
                        {
                            SubCategoryID = Convert.ToInt32(id)
                        };
                        var catdata = _CallAPI.ListSubCategory(itt);
                        for (int i = 0; i < catdata.Count; i++)
                        {
                            if (catdata[i].SubCategoryID == Convert.ToInt32(id))
                            {
                                catinfo.CategoryID = Convert.ToInt32(catdata[i].CategoryID).ToString();
                                catinfo.SubCategoryName = catdata[i].Name;
                                catinfo.IsActive = catdata[i].IsActive.ToString();
                                catinfo.SubCategoryID = Convert.ToInt32(catdata[i].SubCategoryID);
                            }
                        }
                        return View(catinfo);
                    }

                }
               
                catinfo.SubCategoryName = "";
                catinfo.CategoryID = "0";
                catinfo.SubCategoryID = 0;
                return View();
            }
        }

        [HttpPost]
        public ActionResult SubCategory(VM_SubCategory model)
        {
            if(model.SubCategoryName!="" && model.CategoryID!="0")
            {
                var subcat = _CallAPI.ManageSubCategory(model);
                return RedirectToAction("SubCategory", new { id = 0 });
            }
            else
            {
                return PartialView("SubCategory");
            }
        }
        #endregion

        #region Manage Product
        public ActionResult Product(int? id)
        {
            VM_SubCategory catinfo = new VM_SubCategory();
            if (Session["AdminID"] == null)
                return View("Admin");
            else
            {
                if (id != null)
                {
                    if (Convert.ToInt32(id) == 0)
                    {

                        catinfo.SubCategoryName = "";
                        catinfo.CategoryID = "0";
                        catinfo.SubCategoryID = 0;
                        return View(catinfo);
                    }
                    else
                    {
                        VM_SubCategory itt = new VM_SubCategory()
                        {
                            SubCategoryID = Convert.ToInt32(id)
                        };
                        var catdata = _CallAPI.ListSubCategory(itt);
                        for (int i = 0; i < catdata.Count; i++)
                        {
                            if (catdata[i].SubCategoryID == Convert.ToInt32(id))
                            {
                                catinfo.CategoryID = Convert.ToInt32(catdata[i].CategoryID).ToString();
                                catinfo.SubCategoryName = catdata[i].Name;
                                catinfo.IsActive = catdata[i].IsActive.ToString();
                                catinfo.SubCategoryID = Convert.ToInt32(catdata[i].SubCategoryID);
                            }
                        }
                        return View(catinfo);
                    }

                }

                catinfo.SubCategoryName = "";
                catinfo.CategoryID = "0";
                catinfo.SubCategoryID = 0;
                return View();
            }
        }
        #endregion
    }
}