﻿using MTFTrades.Models;
using MTFTrades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace MTFTrades.CommonClasses
{
    public class CallAPI
    {

        public List<VM_SignUp> SignUpNewDistributer(VM_SignUp model)
        {
            List<VM_SignUp> DistributerInfo = new List<VM_SignUp>();
            try
            {
              
                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/SignUp_Distributer";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<VM_SignUp>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            VM_SignUp tm = new VM_SignUp()
                            {
                                SponcerID = list[i].SponcerID.ToString(),
                                Password = list[i].Password
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }

        public List<MHT_Distributer> LoginDistributer(VM_Login model)
        {
            List<MHT_Distributer> DistributerInfo = new List<MHT_Distributer>();
            try
            {

                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/Login_Distributer";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<MHT_Distributer>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            MHT_Distributer tm = new MHT_Distributer()
                            {
                                FirstName = list[i].FirstName.ToString(),
                                LastName = list[i].LastName,
                                DistributerID = list[i].DistributerID
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }

        public List<MHT_Admin> LoginAdmin(VM_AdminLogin model)
        {
            List<MHT_Admin> DistributerInfo = new List<MHT_Admin>();
            try
            {

                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/Login_Admin";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<MHT_Admin>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            MHT_Admin tm = new MHT_Admin()
                            {
                                UserName = list[i].UserName,
                                Password = list[i].Password,
                                AdminID = list[i].AdminID
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }


        public List<MHT_Category> ManageCategory(VM_Category model)
        {
            List<MHT_Category> DistributerInfo = new List<MHT_Category>();
            try
            {

                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/Add_Edit_Category";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<MHT_Category>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            MHT_Category tm = new MHT_Category()
                            {
                              CategoryID=list[i].CategoryID,
                              Name=list[i].Name
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }

        public List<MHT_Category> ListCategory(VM_Category model)
        {
            List<MHT_Category> DistributerInfo = new List<MHT_Category>();
            try
            {

                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/List_Category";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<MHT_Category>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            MHT_Category tm = new MHT_Category()
                            {
                                CategoryID = list[i].CategoryID,
                                Name = list[i].Name,
                                IsActive=list[i].IsActive
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }

        public List<MHT_SubCategory> ManageSubCategory(VM_SubCategory model)
        {
            List<MHT_SubCategory> DistributerInfo = new List<MHT_SubCategory>();
            try
            {

                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/Add_Edit_SubCategory";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<MHT_SubCategory>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            MHT_SubCategory tm = new MHT_SubCategory()
                            {
                                CategoryID = list[i].CategoryID,
                                Name = list[i].Name,
                                IsActive=list[i].IsActive,
                                SubCategoryID=list[i].SubCategoryID
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }

        public List<MHT_SubCategory> ListSubCategory(VM_SubCategory model)
        {
            List<MHT_SubCategory> DistributerInfo = new List<MHT_SubCategory>();
            try
            {

                using (var client = new HttpClient())
                {
                    string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["siteUrl"];
                    string PostUrl = "api/MTFTAPI/List_SubCategory";
                    BaseUrl = BaseUrl + PostUrl;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage responce = client.PostAsJsonAsync(BaseUrl, model).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var list = responce.Content.ReadAsAsync<List<MHT_SubCategory>>().Result;
                        for (int i = 0; i < list.Count; i++)
                        {
                            MHT_SubCategory tm = new MHT_SubCategory()
                            {
                                CategoryID = list[i].CategoryID,
                                Name = list[i].Name,
                                IsActive = list[i].IsActive,
                                SubCategoryID=list[i].SubCategoryID
                            };
                            DistributerInfo.Add(tm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return DistributerInfo;
        }
    }
}