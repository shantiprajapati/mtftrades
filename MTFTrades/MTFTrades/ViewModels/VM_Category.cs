﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTFTrades.ViewModels
{
    public class VM_Category
    {
        public int CategoryID { get; set; }
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Enter Category Name")]
        [Display(Name = "Category Name")]
        [MaxLength(45, ErrorMessage = "Enter Category Name")]
        public string CategoryName { get; set; }

      
        [Required(ErrorMessage = "Select Is Active")]
        [Display(Name = "Is Active")]       
        public bool IsActive { get; set; }
    }
}