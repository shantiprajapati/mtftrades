﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTFTrades.ViewModels
{
    public class VM_SubCategory
    {
        public int SubCategoryID { get; set; }       
        public string CategoryID { get; set; }
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Enter Sub Category Name")]
        [Display(Name = "Sub Category Name")]
        [MaxLength(45, ErrorMessage = "Enter Sub Category Name")]
        public string SubCategoryName { get; set; }      
       
        [Display(Name = "Is Active")]
        public string IsActive { get; set; }
    }
}