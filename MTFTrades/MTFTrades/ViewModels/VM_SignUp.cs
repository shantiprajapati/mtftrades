﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTFTrades.ViewModels
{
    public class VM_SignUp
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Enter Your First Name")]
        [Display(Name = "First Name")]
        [MaxLength(20, ErrorMessage = "Enter First Name Length not more than 20 character")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Enter Your Last Name")]
        [Display(Name = "Last Name")]
        [MaxLength(20, ErrorMessage = "Enter Last Name Length not more than 20 character")]
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Enter Your Mobile Number")]
        [Display(Name = "Mobile No.")]
        [MaxLength(15, ErrorMessage = "Enter mobile Number")]
        public string MobileNumber { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Enter Sponcer ID")]
        [Display(Name = "Sponcer ID")]
        [MaxLength(10, ErrorMessage = "Enter Valid Sponcer ID")]
        public string SponcerID { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Enter Password")]
        [Display(Name = "Password")]
        [MaxLength(10, ErrorMessage = "Enter Valid Password")]
        public string Password { get; set; }
    }
}