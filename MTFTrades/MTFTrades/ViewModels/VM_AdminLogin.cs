﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTFTrades.ViewModels
{
    public class VM_AdminLogin
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Enter User Name")]
        [Display(Name = "User Name")]
        [MaxLength(20, ErrorMessage = "Enter Valid User Name")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Enter Password")]
        [Display(Name = "Password")]
        [MaxLength(10, ErrorMessage = "Enter Valid Password")]
        public string Password { get; set; }
    }
}