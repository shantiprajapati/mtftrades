﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTFTrades.ViewModels
{
    public class VM_Login
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage ="Enter Sponcer ID")]
        [Display(Name ="Sponcer ID")]
        [MaxLength(20,ErrorMessage ="Enter Valid Sponcer ID")]
        public string SponcerID { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Enter Password")]
        [Display(Name = "Password")]
        [MaxLength(10, ErrorMessage = "Enter Valid Password")]
        public string Password { get; set; }
    }
}