﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTFTrades.ViewModels
{
    public class VM_Product
    {
       
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string PurchageRate { get; set; }
        public string SellingRate { get; set; }
        public DateTime MFGDate { get; set; }
        public DateTime EXPDate { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Image { get; set; }
        public string Details1 { get; set; }
        public string Details2 { get; set; }
        public string Details3 { get; set; }
        public string Feature { get; set; }
        public bool IsActive { get; set; }
        
    }
}