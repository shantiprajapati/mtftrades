//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTFTrades.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MHT_PurchageProduct
    {
        public long P_PurchageProductID { get; set; }
        public Nullable<long> PurchageProductID { get; set; }
        public Nullable<long> ProductID { get; set; }
        public Nullable<long> Quantity { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
    }
}
