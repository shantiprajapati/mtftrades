//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTFTrades.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MHT_SubCategory
    {
        public long P_SubCategoryID { get; set; }
        public Nullable<long> SubCategoryID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> DisplayOrder { get; set; }
        public Nullable<long> CategoryID { get; set; }
    }
}
