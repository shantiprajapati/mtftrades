﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MTFTrades
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
               name: "Admin",
               url: "Admin/{action}/{id}",
               defaults: new { controller = "Admin", action = "Admin", id = UrlParameter.Optional }
               );
            routes.MapRoute(
              name: "Shop",
              url: "Shop/{action}/{id}",
              defaults: new { controller = "Shop", action = "Login", id = UrlParameter.Optional }
              );
            routes.MapRoute(
               name: "Distributer",
               url: "Distributer/{action}/{id}",
               defaults: new { controller = "Distributer", action = "DashBoard", id = UrlParameter.Optional }
               );
            //routes.MapRoute(
            //    name: "Home",
            //    url: "{myid}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //    );
            routes.MapRoute("SpecificRoute", "{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
